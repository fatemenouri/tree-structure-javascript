var count = 1;

function addNewDiv(e) {
    parentId = e.target.parentNode.id;
    parent = document.getElementById(parentId);
    child = document.createElement('div');
    child.id = count;
    parent.appendChild(child);
    createButtons(child);
    count = count + 1;
}

function createButtons(child) {

    paragraph = document.createElement('p');
    paragraph.innerHTML = `element id= ${child.id} - parent id =${child.parentNode.id}`;
    addButton = document.createElement('BUTTON');
    addButton.innerHTML = '+'
    addButton.addEventListener("click", addNewDiv);
    subButton = document.createElement('BUTTON');
    subButton.addEventListener("click", subDiv);
    subButton.innerHTML = '-';
    child.appendChild(paragraph);
    child.appendChild(addButton);
    child.appendChild(subButton);
}

function subDiv(e) {
    parentId = e.target.parentNode.id;
    parent = document.getElementById(parentId);
    parent.parentNode.removeChild(parent);
}